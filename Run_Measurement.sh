#!/usr/bin/env bash
mininet_file="Mininet/TestSetups/Four_switches_two_ways_iperfa.py"
while [ "$1" != "" ]; do
    case $1 in
        -h | --help)
            echo "-mn: mininet file path"
            exit
            ;;
        -mn)
            shift
            mininet_file = $1
            ;;
        * )
            echo "Using standard values"
            ;;
        #exit 1
    esac
    # argument shift
    shift
done
# opens mininet in additional terminal
xterm -hold -e ryu-manager Controller/remote_controller.py &
python $mininet_file
mn -c
