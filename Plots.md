# Plotting Guidelines

## 2 Topologies:

s1 10ms     s2  10ms    s4

s1 14ms     s3  14ms    s4

s1 4Mbit/s  s2  4Mbit/s s4

s1 3Mbit/s  s3  3Mbit/s s4



2 hosts each on switch 1 and 4

3 hosts each on switch 1 and 4


## General config settings:
qMode = QMode.Q_LEARNING

alpha = 0.8

gamma = 0.3

epsilon = 0.1

bias = BiasRL.SPF

resetQTestFlag = True

splitUpLoadLevelsFlag = False

load_levels = [10]

exploration_mode = ExplorationMode.CONSTANT_EPS

delay_reward = 2

measurements_for_reward = 1

bw_max_dict = {1: {2: 4.0, 3: 3.0}, 2: {4: 4.0}, 3: {4: 3.0}}

queue_lenght = 50

reward_mode = RewardMode.ONLY_LAT

iterations = 200

bias = BiasRL.SPF


## Plots:

### Q-learning vs. SARSA
#### SARSA
qMode = QMode.SARSA
#### Q-learning
qMode = QMode.Q_LEARNING

### Different Action constellations
#### Direct change of states
action_mode = ActionMode.DIRECT_CHANGE
#### Just one flow
action_mode = ActionMode.ONE_FLOW


### RL vs. SPF (Throughput) -> style of Boyan
#### RL
qMode = QMode.Q_LEARNING
#### SPF
qMode = SHORTEST_PATH


### Exploration:
#### Eps_greedy (varying eps 4 steps)
-- learning speed
-- standard deviation
  epsilon = [0.05, 0.1, 0.15, 0.2]
  
### Different Exploration Strategies:
#### Eps_greedy
exploration_mode = ExplorationMode.CONSTANT_EPS
#### Softmax
exploration_mode = ExplorationMode.SOFTMAX
#### UCB
exploration_mode = ExplorationMode.UCB

### Initialisation
#### Random
bias = BiasRL.RANDOM
#### DFS
bias = BiasRL.SPF

### Scaling Flows, Topo Fest
#### more flows
 Modifying Mininet files
### Scaling Topo, Flows Fest
#### more switches (paths) added
 Modifying Mininet files
 
 
### Merging Q tables
adding flows

### Changing load levels
load_levels = [4, 10]
#### optimistic
  mergingQTableFlag = True
#### reset
  mergingQTableFlag = False


## (Optimal)
Adding BW into state space

Learning and testing

# Storyline
Topologies

n .. number of flows

m .. number of switches parallel

### Pre-study
Orientation on Q-routing

But central view -> Link state

Path searching graph -> slow: plot -> doesn't make sense

### Next steps
#### Selection state space
#### Selection action space
decision between direct and one_flow (plot)
#### Selection algorithm
plot; decision between q_routing and SARSA
#### Exploration algorithms
plot: showing eps_greedy varying eps

plot: showing eps_greedy falling eps

plot: showing UCB, softmax, UCB
#### Table with all values
#### Average delay comparison SPF (style of boyan)
#### over time -> average delay SPF vs. scheme
#### Varying initialisation
#### Varying load level
write down definition load level

showing load level change 4 -> 10
#### Idea: Merging Q-tables
plot: flow that does not congest

plot: flow that congests

#### Extensions (optimal):
- real routing: state space is depending on number of flows

-- calculation

-- deciding where to put next

--> thinking about setup

- including bw into state space

- other topology