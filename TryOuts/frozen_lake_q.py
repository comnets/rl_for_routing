import gym
import numpy as np
import random
import pickle
import copy
from enum import Enum

class ExplorationMode(Enum):
  CONSTANT_EPS = 0
  #FALLING_EPS = 1
  SOFTMAX = 2
  UCB = 3

def choosing_eps_greedy(Q, state, eps):
    possibleMoves = [0, 1, 2, 3]
    bestMove = np.argmax(Q[state])
    if random.random() < epsilon:
        possibleMoves.pop(bestMove)
        aIndex = random.choice(possibleMoves)
        a = int(aIndex)
    else:
        a = bestMove
    return a

def choosing_UCB(Q, times_visited, state, c):
    """
    returns action
    :param Q:
    :param times_visited:
    :param state:
    :param c:
    :return:
    """
    values = {}
    actions = [0,1,2,3]
    times_visited_total = sum(times_visited[state])
    for action in actions:
        if times_visited[state][action] == 0:
            return action
        values[action] = Q[state][action] + c * np.sqrt(2 * np.log(times_visited_total) / times_visited[state][action])
    return max(values, key=values.get)

def softmax(q_value, beta=1.0):
    assert beta >= 0.0
    q_tilde = q_value - np.max(q_value)
    factors = np.exp(beta * q_tilde)
    return factors / np.sum(factors)

def choosing_softmax(q_value, curr_s, beta=1.0):
    prob_a = softmax(q_value[curr_s, :], beta=beta)
    cumsum_a = np.cumsum(prob_a)
    return np.where(np.random.rand() < cumsum_a)[0][0]

def choosing_action(Q, times_visited, state, method, eps, temp, c):
    if method == ExplorationMode.UCB.value:
        return choosing_UCB(Q, times_visited, state, c)
    if method == ExplorationMode.SOFTMAX.value:
        return choosing_softmax(Q, state, temp)
    if method == ExplorationMode.CONSTANT_EPS.value:
        return choosing_eps_greedy(Q, state, eps)

#meas = [[0.05,0.5,0.5],[0.1,0.75,1],[0.15,1,2],[0.2,2,3],[0.25,3,5],[0.3,5,10]]
num = 5
env = gym.make("FrozenLake-v0")
#Initialize table with all zeros
learning_rate = 0.8
gamma = 0.9
n_episodes = 1000000
temp = 2
c = 50
epsilon = 0.1
n_iterations = 100
reward_list_list = []
reward_list = []
m = 'CONSTANT_EPS'
for k in range(n_iterations):
    Q = np.zeros([env.observation_space.n, env.action_space.n])
    times_visited = np.zeros([env.observation_space.n, env.action_space.n], dtype=np.int64)
    for i in range(n_episodes):
        state = env.reset()
        reward_total = 0
        reached_dest = False
        j = 0
        # The Q-Table learning algorithm
        # choosing action greedy
        while j < 99:
            j += 1
            a = choosing_action(Q, times_visited, state, ExplorationMode[m].value, epsilon, temp, c)
            # Get new state and reward from environment
            s1, reward,reached_dest,_= env.step(a)
            Q[state, a] = Q[state, a] + learning_rate * (reward + gamma * np.max(Q[s1, :]) - Q[state, a])
            times_visited[state, a] += 1
            state = s1
            reward_total = reward_total + reward
            if reached_dest == True:
                break
        reward_list.append(reward_total)
    reward_list_list.append(copy.deepcopy(reward_list))
    reward_list.clear()
    if k % 10 == 0:
        print("Progress in {}: {}".format(m, k/n_iterations))
    print(Q)
    #with open('../Plots/list_reward_{}_{}.txt'.format(m, num), 'w') as fp:
    #    pickle.dump(reward_list_list, fp)
    #print("Saving list")
#with open('../Plots/list_reward_{}_{}.txt'.format(m, num), 'wb') as fp:
#    pickle.dump(reward_list_list, fp)

