import math
import matplotlib.pyplot as plt
import numpy as np
import plotting

plotting.setup(span=False)

taus = [0.05,  0.001, 0.0005, 0.005, 0.0003]
#taus = [0.5,1,1.5,2,5]
#taus = [0.05, 10**(-2),0.001,0.005,0.002,0.003, 0.0005, 0.0075,0.0008]
measurementsArray = []
fig, ax = plt.subplots()
for tau in taus:
    print(tau), 10**(-3)
    i =  -5*10**(2)
    x = []
    y = []
    while i < -0.1:
        actions = [i, -200, -200]
        #actions = [i, -400, -400, -400]
        # print(i,[math.exp( -1 / (action * tau)) for action in actions])
        total = sum([np.exp( -1 / (action * tau), dtype=np.float128) for action in actions])
        probs = [(np.exp(-1 / (action * tau), dtype=np.float128) / total) for action in actions]
        #total = sum([np.exp( (action / tau), dtype=np.float128) for action in actions])
        #probs = [(np.exp((action / tau), dtype=np.float128) / total) for action in actions]
        #print("tau {} probs: {}".format(tau, probs))
        x.append(i)
        y.append(probs[0])
        i = i + 0.1
    ax.plot(x, y, label=r'$\tau$ = {}'.format(tau))

ax.set_xlim(-500,0)
ax.set_xlabel('$u$')
ax.set_ylabel(r'Probability of selecting action $a_u$')
ax.set_title(r'Action selection probabilities using the modified Softmax for $u[a_{u}, -200, -200]$')
plt.tight_layout()
ax.legend()
plt.savefig('softmax_neg.pdf')
plt.show()