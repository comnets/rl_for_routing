import collections
import numpy as np
from heapq import *
import math

def iterative_levenshtein(s, t):
    """
        iterative_levenshtein(s, t) -> ldist
        ldist is the Levenshtein distance between the strings
        s and t.
        For all i and j, dist[i,j] will contain the Levenshtein
        distance between the first i characters of s and the
        first j characters of t
    """
    rows = len(s) + 1
    cols = len(t) + 1
    dist = [[0 for x in range(cols)] for x in range(rows)]
    # source prefixes can be transformed into empty strings
    # by deletions:
    for i in range(1, rows):
        dist[i][0] = i
    # target prefixes can be created from an empty source string
    # by inserting the characters
    for i in range(1, cols):
        dist[0][i] = i

    for col in range(1, cols):
        for row in range(1, rows):
            if s[row - 1] == t[col - 1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = min(dist[row - 1][col] + 1,        # deletion
                                 dist[row][col - 1] + 1,         # insertion
                                 dist[row - 1][col - 1] + cost)  # substitution
    return dist

def heuristic(a, b):
    #return (b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2
    #euclidian
    return math.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)

def astar(array, start, goal):

    #neighbors = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1)]
    #neighbors = [(0, -1), (-1, 0),  (-1, -1)]
    # with diagonal
    neighbors = [(0, 1), (1, 0), (1, 1)]
    #without diagonal
    #neighbors = [(0, 1), (1, 0)]
    close_set = set()
    came_from = {}
    gscore = {start: 0}
    fscore = {start: heuristic(start, goal)}
    oheap = []

    heappush(oheap, (fscore[start], start))

    while oheap:
        current = heappop(oheap)[1]
        if current == goal:
            data = []
            while current in came_from:
                data.append(current)
                current = came_from[current]
            return data
        close_set.add(current)
        for i, j in neighbors:
            neighbor = current[0] + i, current[1] + j
            tentative_g_score = gscore[current] + heuristic(current, neighbor)
            #print(array.shape[0])
            #print(array.shape[1])
            if 0 <= neighbor[0] < array.shape[0] and 0 <= neighbor[1] < array.shape[1]:
                if neighbor in close_set and tentative_g_score >= gscore.get(neighbor, 0):
                    continue
                if tentative_g_score < gscore.get(neighbor, 0) or neighbor not in [i[1] for i in oheap]:
                    came_from[neighbor] = current
                    neighbor_difference = float(array[neighbor[0]][neighbor[1]])
                    gscore[neighbor] = tentative_g_score + neighbor_difference
                    fscore[neighbor] = tentative_g_score + heuristic(neighbor, goal)
                    heappush(oheap, (fscore[neighbor], neighbor))
    return False
#ABCDEFGHIJKLMOP
#old = ['1', '2', '3', '4', '5', '6', '11', '12', '13', '16', '18', '12']
#new = ['1', '6', '3', '7', '8', '9', '10', '11', '14', '15']
#old = ['1','3','2','4','5']
#new = ['1','3','4']
#old = ['1','2']
#new = ['1','2','4']
#old = ['1','2','4']
#new = ['1','3','2','4']
#old = ['1','2','4']
#new = ['1','3','2','4']
old = ['1','2','3','4','5']
new = ['1','3','5']
npArray = np.array(iterative_levenshtein(new, old))
print(npArray)
# a star to find out operations
#aStar = astar( npArray, (len(first), len(second)),(0,0) )
aStar = astar(npArray, (0,0), (len(new), len(old)))
aStar.append((0, 0))

print(aStar)

# analyze:
i = 0
# tuple: (operation, index)
changelist = []
# 0 - no change
# 1 - substitution
# 2 - insert
# 3 - delete
# numpy: (zeile, spalte)
for nextElement in aStar:
    if i > 0:
        previousElement = aStar[i-1]
        # aenderung spalte deletion
        if (nextElement[1] < previousElement[1] and nextElement[0] < previousElement[0]):
            # no change
            if (npArray[nextElement[0]][nextElement[1]] == npArray[previousElement[0]][previousElement[1]]):
                changelist.append((0, previousElement[0], previousElement[1]))
            else:
                changelist.append((1, previousElement[0], previousElement[1]))
        elif (nextElement[1] == previousElement[1] and nextElement[0] < previousElement[0]):
            changelist.append((2, previousElement[0], previousElement[1]))
        elif(nextElement[1] < previousElement[1] and nextElement[0] == previousElement[0]):
            changelist.append((3, previousElement[0], previousElement[1]))
    i += 1

# command, switch number
print(changelist)
opPrev = 0
changeInsert = []
changeInsertLast = []
changeDelete = []

for change in changelist:
    operation = change[0]
    indexOld = change[2] - 1
    indexNew = change[1] - 1

    if operation == 0:
        print('noChange indexold: {}  Indexnew: {}'.format(indexOld, indexNew))
    elif operation == 1:
        # put it into changelast
        print('substitute: {} from {}'.format(indexOld, indexNew))
        old[indexOld] = new[indexNew]
        opPrev = 1
    elif operation == 2:
        print('insert from {} to {}'.format(indexOld, indexNew))
        old.insert(indexOld + 1, new[indexNew])
        opPrev = 1
    elif operation == 3:
        print('delete from: {}'.format(indexOld))
        old.pop(indexOld)
        opPrev = 1
print(old)
print(new)
# check if right

