import pandas as pd
import json
import matplotlib.pyplot as plt

dtype = {"state": "str", "latency": "float", "timestamp": "float"}
dtypenames = ["state", "latency", "timestamp"]

dirStr = "state_avg_lat.csv"

data = pd.read_csv(dirStr, delimiter=';', names=dtypenames, dtype=dtype, skiprows=1)

states = data['state']
latencies = data['latency']

i = 0

latDict = {}
for state in states:
    if state not in latDict:
        latDict[state] = []
    latDict[state].append(latencies[i])
    i += 1

optimal_state1 = '{"10.0.0.10_10.0.0.2": [10, 12, 7, 3, 2], "10.0.0.11_10.0.0.2": [11, 9, 8, 5, 2], "10.0.0.7_10.0.0.2": [7, 6, 4, 1, 2], "10.0.0.7_10.0.0.9": [7, 12, 11, 9]}'
optimal_state2 = '{"10.0.0.10_10.0.0.2": [10, 9, 8, 5, 2], "10.0.0.11_10.0.0.2": [11, 12, 7, 3, 2], "10.0.0.7_10.0.0.2": [7, 6, 4, 1, 2], "10.0.0.7_10.0.0.9": [7, 12, 11, 9]}'
print(len(latDict[optimal_state1]))
print(len(latDict[optimal_state2]))

fig, ax = plt.subplots()
fig2, ax2 = plt.subplots()
fig3, ax3 = plt.subplots()
ax.set_title('First Optimal state, theory: {}'.format(18.75))
ax2.set_title('Second Optimal state, theory: {}'.format(18.75))
print(latDict[optimal_state1])
ax.boxplot(latDict[optimal_state1])
ax2.boxplot(latDict[optimal_state2])
ax3.plot(latDict[optimal_state2])
fig.savefig('first_optimal.pdf')
fig2.savefig('scnd_optimal.pdf')
fig3.savefig('over_time.pdf')
plt.show()
