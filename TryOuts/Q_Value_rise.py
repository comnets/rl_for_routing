import matplotlib.pyplot as plt

alpha = 0.8
gamma = 0.8
rewards = [-20, -40, -75, -110]

j=0


ericsonColors =  [(0, 169, 212), (240, 138, 0), (227, 34, 25), (250, 187, 0),
                 (165, 199, 83), (233, 92, 56),(0, 123, 120),
                 (245, 162, 65),(0,40, 95), (143, 63, 123)]
ericsonColorsNormed = [(a[0]/255, a[1]/255, a[2]/255) for a in ericsonColors]
for reward in rewards:
    q = 0
    q_array = []
    q_array = [0]
    for i in range (1,100):
        q = q + alpha * (reward + gamma * q - q)
        q_array.append(q)
    print("Snd for {}: {} fith:{} Last: {}".format(reward, q_array[2],q_array[5], q_array[-1]))
    plt.plot(q_array, color=ericsonColorsNormed[j], label=r'$r = {}$'.format(reward))
    j = j + 1
plt.xlabel("Learning Steps")
plt.ylabel(r'$Q(s,a)$')
plt.xlim(0, 50)
plt.legend()
plt.savefig('Q_values_over_time.pdf')
plt.show()