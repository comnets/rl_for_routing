import math
import matplotlib.pyplot as plt
import plotting

import numpy as np
ericsonColors = [(0, 169, 212), (240, 138, 0), (250, 187, 0),
                 (165, 199, 83), (233, 92, 56),(0, 123, 120), (227, 34, 25),
                 (245, 162, 65),(0,40, 95), (143, 63, 123)]
#+plt.style.use('ggplot')
plotting.setup(span=False)
fig, ax = plt.subplots()


#plt.set_cmap('jet')
taus = [0.1, 0.5, 1, 2, 10]
measurementsArray = []

for tau in taus:
    i = 0
    x = []
    y = []
    while i < 5:
        actions = [i, 1,1]
        total = sum([np.exp((action) / tau) for action in actions])
        probs = [(np.exp(action / tau) / total) for action in actions]
        x.append(i)
        y.append(probs[0])
        i = i + 0.0001
    ax.plot(x, y, label=r'$\tau$ = {}'.format(tau))

#ax.set_xlim(xmin=0)
#ax.set_ylim(ymin=0)
ax.legend()
ax.set_xlabel('Value of $u$')
ax.set_ylabel(r'Probability of selecting $a_{u}$')
ax.set_xlim(0, 5)
ax.set_title(r'Action selection probabilities using Softmax for $u[a_{u}, 200, 200]$')
plt.tight_layout()
plt.savefig('softmax_pos.png')
#print()
plt.show()