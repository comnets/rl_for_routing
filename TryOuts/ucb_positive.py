import math
import matplotlib.pyplot as plt
import numpy as np
import random
numActions = 3
c = 0.5
n = 10
i = 0
prob = {}
num = {}
for j in range(1, n+1):
    prob[j] = 1 / n
    num[j] = 0
# If N t (a) = 0, then a is considered to be a maximizing action !!!
for i in range(n):
    pv = list(prob.values())
    print(pv)
    chosenKey = np.random.choice(list(prob.keys()), p = list(prob.values()))
    prob[chosenKey] = c * np.sqrt(2 * np.log(i) / num[chosenKey])
    num[chosenKey] = num[chosenKey] + 1

print("num: {}".format(num))
print("prob: {}".format(prob))