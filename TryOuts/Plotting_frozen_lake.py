import pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import copy
import pandas as pd
from matplotlib import rc
import plotting
def setAxes(xLim = 50000):
    a=0
rewardLists = []
meas = [[0.05,0.5,0.5],[0.1,0.75,1],[0.15,1,2],[0.2,2,3],[0.25,3,5],[0.3,5,10]]
ericsonColors = [(0, 169, 212), (240, 138, 0), (227, 34, 25), (250, 187, 0),
                 (165, 199, 83), (233, 92, 56),(0, 123, 120),
                 (245, 162, 65),(0,40, 95), (143, 63, 123)]

colors = [(a[0]/255, a[1]/255, a[2]/255) for a in ericsonColors]
#for i in range(1,6):
eps_dict = {}
sm_dict = {}
ucb_dict = {}
plotting.setup(span=False)

for i in range(0, 5):
    epsilon = meas[i][0]
    c = meas[i][1]
    temp = meas[i][2]
    with open('plotting_sm.txt', 'rb') as infile:
        sm_dict = pickle.load(infile)
    with open('plotting_eps.txt', 'rb') as infile:
        eps_dict = pickle.load(infile)
    with open('plotting_ucb.txt', 'rb') as infile:
        ucb_dict = pickle.load(infile)
    print("loaded all")
cols = 2
rows = 3
figsizeLeft = (50000, 5)
fig1, axs1 = plt.subplots(2, 1, constrained_layout=True)
fig2, axs2 = plt.subplots(2, 1, constrained_layout=True)
fig3, axs3 = plt.subplots(2, 1, constrained_layout=True)
i = 0
for key in sm_dict:
    meanList = sm_dict[key]
    cuttedMean = meanList[0:50000]
    cuttedList = meanList[0:1000]
    axs1[0].plot(copy.deepcopy(cuttedMean), label=r'$\tau$ = {}'.format(key), color = colors[i])
    axs1[1].plot(copy.deepcopy(cuttedList), label=r'$\tau$ = {}'.format(key), color = colors[i])
    axs1[0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.25),
          fancybox=True, shadow=True, ncol=5)
    axs1[1].set_xlabel("episodes")
    axs1[0].set_ylabel("reward")
    axs1[1].set_ylabel("reward")
    if i > 2:
        break
    i = i + 1
i = 0
for key in eps_dict:

    meanList = eps_dict[key]
    cuttedMean = meanList[0:50000]
    cuttedList = meanList[0:1000]
    axs2[0].plot(copy.deepcopy(cuttedMean), label=r'$\epsilon$ = {}'.format(key), color = colors[i])
    axs2[1].plot(copy.deepcopy(cuttedList), label=r'$\epsilon$ = {}'.format(key), color = colors[i])
    axs2[0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.25),
                   fancybox=True, shadow=True, ncol=5)
    axs2[1].set_xlabel("episodes")
    axs2[0].set_ylabel("reward")
    axs2[1].set_ylabel("reward")
    if i > 2:
        break
    i = i + 1
i = 0
for key in ucb_dict:
    meanList = ucb_dict[key]
    cuttedMean = meanList[0:50000]
    cuttedList = meanList[0:1000]
    axs3[0].plot(copy.deepcopy(cuttedMean), label=r'$c$ = {}'.format(key), color = colors[i])
    axs3[1].plot(copy.deepcopy(cuttedList), label=r'$c$ = {}'.format(key), color = colors[i])
    axs3[0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.25),
                   fancybox=True, shadow=True, ncol=5)
    axs3[1].set_xlabel("episodes")
    axs3[0].set_ylabel("reward")
    axs3[1].set_ylabel("reward")
    if i > 2:
        break
    i = i + 1
#print("Len: {}".format(len(rewardLists)))
#fillUpList = np.mean(np.array(rewardLists), axis=0)
fig1.tight_layout()
fig2.tight_layout()
fig3.tight_layout()

#rc('text', usetex=True)
#print(ucb_dict)
fig1.savefig("frozen_lake_sm.pdf")
fig2.savefig("frozen_lake_eps.pdf")
fig3.savefig("frozen_lake_ucb.pdf")
#plt.show()