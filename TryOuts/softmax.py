import math
z = [1.0, 2.0, 3.0, 10.0, 1.0, 2.0, 3.0]
z_exp = [math.exp(i) for i in z]
sum_z_exp = sum(z_exp)
softmax = [i / sum_z_exp for i in z_exp]
print([round(i, 3) for i in softmax])