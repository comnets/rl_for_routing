import copy
chosenflowPrev = [1, 7, 4, 3, 6, 8, 5, 9]
newPath =        [1, 2, 4, 7, 6, 9]
i = 0
flowAddList = []
flowModList = []
flowDeleteList = []
chosenflowPrevCopy = copy.copy(chosenflowPrev)

differenceSet = set(chosenflowPrev).difference(newPath)
# check if things deleted
if len(differenceSet) > 0:
    flowDeleteList = list(differenceSet)

for switch in newPath:
    print("Checking Switch (new): {}".format(switch))
    if switch in chosenflowPrev:
        #TODO: obsolete
        chosenflowPrevCopy.remove(switch)
        # check prev
        indexPrev = chosenflowPrev.index(switch)
        if(i>0):
            if newPath[i-1] == chosenflowPrev[indexPrev-1]:
                i += 1
                continue
            # have to change index before
            else:
                if (( newPath[i-1] not in flowAddList)) and ((newPath[i-1] not in flowDeleteList) and (switch not in flowDeleteList)):
                    print("Not same: {}".format(switch))
                    #flowModList.append(chosenflowPrev[indexPrev-1])
                    flowModList.append(newPath[i - 1])
    else:
        flowAddList.append(switch)
        print("switch not in list: {}".format(switch))
        indexPrev = newPath.index(switch)
        flowModList.append(newPath[indexPrev-1])
    i += 1
#flowDeleteList = chosenflowPrevCopy
for j in range(0,len(flowDeleteList),1):
    switchOldIndex = chosenflowPrev.index(flowDeleteList[j])
    switchOldIndexPrev = switchOldIndex - 1
    if chosenflowPrev[switchOldIndexPrev] not in flowDeleteList:
        flowModList.append(chosenflowPrev[switchOldIndexPrev])
    j += 1

print("FlowAddList: {}".format(flowAddList))
print("FlowModList: {}".format(flowModList))
print("FlowDelList: {}".format(flowDeleteList))
