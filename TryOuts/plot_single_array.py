import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import json
list_ = []
with open('state_1.txt', 'r') as f:
       data = json.load(f)
f.close()
print(data)
subarray = []
for item in data:
       subarray.append(item[3])
# Data for plotting
t = np.arange(0, len(subarray), 1)
print(subarray)
title = "State 5"
fig, ax = plt.subplots()
#print(t)
ax.plot(t, subarray)
ax.set(xlabel='episodes', ylabel='rewards',
      title=''.format(title))
ax.grid()

#fig.savefig("test.png")
plt.show()