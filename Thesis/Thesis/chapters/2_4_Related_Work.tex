\section{Related Work}
In the scope of this thesis, a data driven approach for routing based on \acrshort{rl} is developed.
Combining machine learning methods with routing and traffic engineering were early in the focus of research, beginning in the 1990s by Boyan and Littmann with their \textit{Q-routing}~\cite{RL_boyan} algorithm. 
At this time, no central observer existed in a communication system\cite{kumar}.
This changed with the introduction of the concept of \acrlong{sdn}. 
For an \acrshort{sdn} controller it is possible to monitor the metrics of the network and directly influence the network by modifying the flow table.
Due to the larger amount of information available, it is possible to perform a more precise optimization according to different objectives.
On the one hand, this led to approaches that were applied to different models and solution methods.
On the other hand, the focus is on novel approaches based on machine learning methods such as supervised or reinforcement learning.
First, the different current methods for traffic engineering and routing are briefly described. 
If existing, they are followed by their implementation into \acrshort{sdn} networks.
Finally, approaches based on machine learning are presented.
For routing, as described in \ref{section:link_state_distance_vector}, it is necessary to distinguish based on the information base.
As a reminder, in a network, if a forwarding device gets the view over the network only based on its neighbors and it performs it routing decisions on that, we refer to distance-vector algorithms.
If the routing is based on the whole topology, which can be converted into a graph, it is called link-state routing.
The focus lies clearly on approaches and protocols that are classifiable as link-state routing, due to the centralized authority and view of the \acrshort{sdn} controller.
There are different approaches to solve the routing problem formulated in \ref{section:routing_problem}.
How the routing is performed depends also if it is performed when a new flow joins the network, so if a connection is built between hosts.
That happens if a user to server communication or even a server to server communication is demanded.
Therefore this type is called \textit{on-demand routing} in this work.
Another type is to determine the optimal path configurations before or after the routes have been established, referred to as \textit{pre- and postcomputed}.
At last an overview of applications of machine learning, especially \acrlong{rl}, in \acrshort{sdn} is given. 

\newpage
\subsection{On-demand} \label{section:rw_on_demand}\peter{vll anders nennen}
When a new route (i.e. flow) needs to be created through the network, the requirement is that the route is calculated as fast as possible.
Because of this, the common routing protocols \acrshort{ospf}~\cite{rfc_ospf} and  IS-IS \peterimportant{cite!!! (when added to introduction} rely on fast path search algorithms like Dijkstra~\cite{dijkstra_performance}.
If the route is only calculated based on the Hop-Count, the algorithm ignores fully the conditions in the link and metrics such as the delay or the available bandwidth.
As it is possible that the least hops do not mean the least end-to-end delay, that could lead to inefficient routing decision.
Ignoring the link conditions can result in congestion or as it would be the case for TCP connections, in lower throughput.
To tackle these problems, as mentioned before, the \acrshort{qos} extension have been introduced \cite{rfc_ospf_extensions_qos} but they it is often not implemented due to the interference of other network parts and routing loops.
A more advanced approach is to expand the shortest path routing with adding constraints.\\
The so called \acrfull{csp} routing minimizes or maximizes a an end-to-end metric while being constrained by another metric.
That can be extended to the \acrfull{mcsp} routing which has to comply with bounds of multiple metrics.
Joksch delivered in \cite{CSP_first} the first formulation of the CSP problem based on integer linear programming.
Therefore it is possible to solve it by dynamic programming~\cite{dyn_prog_integer_constraint}.\\
A simple approach to solve the CSP and MCSP problem is the Fallback algorithm, introduced in \cite{csp_mscp_lee}. 
It computes the path with the least cost and then verifies if the constraints have been met.
The algorithm continuous to calculate paths by exchange the metrics and constraints until a suitable path is found.
Another approach is to enumerate through all possible combinations of paths and select the best solution that satisfies all the constraints.
This would lead to high computational complexity.
In \cite{mcsp_implicit_enamuration}, Aneja \textit{et al.} use implicit programming~\cite[p. 297-300]{applied_math_bradley1977_integer_prog} which gives the possibility to evaluate systematically all solutions without the necessity of evaluating all of them explicitly by fixing them. \\
An algorithm called Constrained Bellman-Ford (CBF) is introduced in \cite{constrained_BF}.
The algorithm has the capabilities to discover possible paths to a set of destination nodes while complying with the destination's constraint.
In contrast to it's name, the algorithm is based on the breadth-first search (BFS) search algorithm~\cite[p. 594-597]{RL_DP_ALGO}.
The algorithm finds independent minimum cost paths between one source and a set of destination nodes subject to each destination’s delay constraint. 
As CBF returns a set of possible paths for each destination node, it can be used for the subsequent optimization.
Another advantage is that paths to multiple destinations are returned.
In the case of unicast  connections, in the case of for example a server and multiple clients, it could relieve the burden of calculating the paths for each client separately.
Especially for multicast connections, the usage of CBF would be beneficial.\\
Another approach to solve the \acrshort{mcsp} problem is an algorithm called A*Prune~\cite{a_prune}, which was developed by Liu and Ramakrishnan based on the A* (A-star) search~\cite{a_star}.
A* relies on a guess-function which defines the nodes chosen which are expected to lead to the goal as fast as possibles.
In A*Prune, it is assumed that a guess function can be adopted for all cost and constraints, in other words the network metrics.
It constructs a heap~\cite[p. 151-152]{RL_DP_ALGO}, so a data structure which also defines the priority of the data.
From this, a priority queue can be derived that is then used for path discovery.
The constraint values are projected and if they exceed their end-to-end bound, they are removed from the queue.
After all, when the destination node is reached, the \acrshort{mcsp} is found.
One major advantage of A*Prune is that it finds \acrshort{mcsp}s to other destination nodes on the way and the search can also be extended to find even more complying paths to other nodes.\\
Other approaches are based on Genetic Algorithms, an approach inspired by nature to solve complex optimization problems.
In \cite{ga_sp}, a genetic algorithm is used for the basic routing problem in networks.
Ahn~\cite{ga_sp_cons} and Hamed~\cite{ga_sp_cons_2} are using this kind of algorithms for solving the routing problem with constraints (i.e. \acrshort{csp}).
For another kind of solutions, the Ant Colonization Optimization method~\cite{Ant_survey} is used.
It is derived by the idea that a collection of ants can find efficiently the shortest path to a source of food and then informing other ants by the deposition of pheromones.
This behaviour can be applied, as shown firstly by Colomi in \cite{Ant_original}, for determining the shortest path in a network.
An approach presented by Di~Caro~\cite{ant_pkt_sw} does apply the optimization based on the behaviour of ants to the routing in packet switched networks.
There are plenty of solutions to find the shortest path based on Genetic Algorithms, Ant Colony Optimization or other optimization methods, but this would be out of the scope of this work.\\
A good overview for unicast routing algorithms in \acrlong{sdn}s delivers \cite{routing_overview}.
However, \acrshort{qos} routing algorithms are commonly greedy, meaning that they attempt to find a path that meets with the particular constraints, but ignoring the impact of their decisions on the whole network. 


\subsection{Pre- and postcomputed} \label{rw_te}
The state of the network depends on the flows and their required bandwidth. 
If a new flow joins, the network does not have the knowledge how much bandwidth the flow wants to obtain.
It is also possible that traffic rates change over time depending on the user behaviour.
Therefore it is beneficial to optimize the network with the metrics of its current state.
Several objectives are common to be chosen for the optimization.
That includes \gls{congestion} Minimization, End-to-End delay Minimization , Packet Loss Minimization, network utility maximization or even Energy consumption Minimization~\cite{te_sdn_survey}.\\
In this work, the focus lays mainly on the congestion and End-to-End delay minimization.
To prevent \gls{congestion}, different techniques can be applied.
One is that different streams share the network resources by minimizing the utilization of the links by distributing the traffic over the network and avoiding congestion.
To achieve a higher accuracy, the traffic flows could be split up on different paths.\\
In \cite{te_traffic_splitting}, Wang \textit{et al.} calculate link weights for arbitrary flow splitting by using linear programming.
Another is to block the access to congested network resources (i.e. links or forwarding devices).
If a new demand is processed, congested resources are not taken into account for the path selection.
Congestion would mean a higher end-to--end delay (i.e. latency) and packet losses.
In other words, congestion minimization has a direct beneficial impact on the other two objectives and can considered as an useful general objective.
But it also has to be noted that the distribution of the traffic can lead to higher latency, so depending on the situation, it can end up in a trade-off.
As a result, proposed approaches generally concentrate on one independent objective.\\
Wang \textit{et al.} propose in \cite{congestion_minimization} a linear programming formulation to prevent congestion by minimizing the maximum of link utilization.
In \cite{congestion_minimization_2}, Trimintzios \textit{et al.} formulate a the problem in a way that they optimize for a lower delay and prevent overloading parts of the network at the same time. 
Additionally, constraints on the hop count and packet losses have been added to ensure meeting the \acrshort{qos} requirements.\\
Next to the solutions based on a multi-constrained problem formulation, others focus on modelling the network with queuing theory~\cite{queue_1}\cite{queue_2}.
Using queuing theory, the complexity grows with the size of the network, therefore multi-hop and multi-point to multi-point networks are still open problems~\cite{nw_moddelling_insufficient}.\peterimportant{vll noch eine Quelle}
In addition, traffic is often assumed as a distribution, often the Poisson distribution~\cite{nw_poisson_insufficient}.
However, this does not always correspond with the reality in complex networks such as the Internet where user demands and link usages are hard to predict.\\
A detailed summary of the application of different approaches to solve \acrshort{te} problems can be found in \cite{te_overview} and their application for \acrshort{sdn} in \cite{te_overview_2}.


\subsection{Industrial Applications} \label{section:rw_industrial}
With a rising traffic requirements through video streams and cloud application led to an increasing number of data centers of content providers distributed around the globe.
These datacenters are connected via separated \acrshort{wan} networks.
These networks are usually expensive and the content provider, usually considerably large enterprise, tend to achieve a high utilization for cost savings.\\
Jain and his colleagues at Google describe in \cite{google_SWAN} how they move away from costly overprovisioning, in their case 2-3 times the necessary bandwidth, to a near perfect utilization of their backbone network B4.
Their network can handle standard routing protocols and has a \acrshort{te} application running on a \acrshort{sdn} controller.
It splits application flows up (i.e.  Multipath forwarding) and allocate these while balancing priority and demands of the different application.
An algorithm based on max-min fairness~\cite{max_min}\cite[p. 8]{mathematics_of_Internet_congestion_control}, a model for the fair allocation of network resources in which it is tried to maximize the minimum rates.
But the algorithm was extended to additionally satisfy the throughput demand of certain applications with prioritization and it can dynamically relocate bandwidth if link failures occur.
Their success by achieving near $100\%$ utilization in some links and the associated cost savings led to more research attraction on \Gls{openflow} and \acrshort{sdn}~\cite{te_sdn_survey}.
Microsoft has similar as a content provider similar to Google's B4, a backbone network connecting his datacenters.\\
Therefore, Huang \textit{et al.} developed a system, called SWAN~\cite{microsoft_SWAN}, to improve the utilization depending on the current traffic demand.
It satisfies its set goals of max-min fairness and strict priority classes for applications by coordinating the sending rates of its services and a beneficial allocation of the data flows.
It derives the flows into different classes and reserves the shortest path always for the ones high priority.
Flows with lower priorities can be disaggregated (i.e. split up) or reallocated to achieve a high utilization.
To prevent congestion due to incoming demands or ongoing traffic allocation, the algorithm leaves a scratch space (i.e. free space dedicated to temporal use) in each link. 
With the usage of their developed algorithm, they achieved a $60\%$ higher utilization.\\
Another content provider which delivers different types of media with the help of distributed datacenters is Facebook.
Edge Fabric~\cite{facebook_edge_fabric} is an \acrshort{sdn}-based system presented by Schlinker \textit{et al.} which manages the traffic between their points of presence, in other words their datacenters.
The main goal of Edge Fabric is to avoid congestion by monitoring the network state and allocate the traffic on alternative paths.
The strong interest of reputable companies in this topic shows its relevance and the demand for a suitable solution.

\subsection{Machine Learning}
The algorithms and approaches described previously are on the one hand based on optimization with constraints or on complicated models that scale poorly.
Machine Learning techniques have shown in previous publications and use cases that they can handle relatively large optimization problems.\\
One common example especially for \acrlong{rl} is the mastering of different games such as Go or Chess~\cite{mot_rl_games}.
The special aspect is that they teach themselves the games, so they are capable to play effectively without the development of an algorithm.
In combination with deep neural networks (i.e. \acrfull{drl}), it can even handle arcade games~\cite{mot_drl_games}.
Other areas include autonomous driving~\cite{mot_drl_driving}, robotics~\cite{drl_robotics} and the financial sector~\cite{mot_rl_financial}.
Also classic control problems such as temperature regulation of facilities~\cite{drl_hvac} can be managed successfully.
A comprehensive overview can be found in \cite{mot_drl_survey_2}.
The advantages of learning to behave effectively in an environment without requiring a model suits well for the dynamic and complex behaviour of todays networks.
In the following paragraph, approaches that use machine learning for routing or traffic engineering are introduced.\\
%\subsection{Routing and Traffic Engineering}
An early occurrence of an application of machine learning for routing was the approach of Boyan and Littman~\cite{RL_boyan}.
In their algorithm, called \textit{Q-routing}, they use the Q-learning algorithm, as introduced in section \ref{q_leanring}.
A reinforcement learning module (i.e. a \acrshort{rl} agent) is implemented into each node of the network.
Information and statistics about the delivery times were gathered, stored locally in each node and used as a reward.
By implementing the Q-learning algorithm, each node estimates the delivery time in the number of hops of a packet until it reaches its destination when sending it to a specific neighbor node.
For evaluation, a discrete network simulator which can forward one packet each timestep.
The Q-tables are predicting the delivery time, showing the similarities to the Bellman-Ford algorithm, that also keeps a cost function in a table.
In Q-routing, the Routing policy is characterized by the Q-values stored in the Q-tables of each node in the network.
For exploration, exploration algorithms  were not used intentionally because of their negative influence on the network state by provoking congestion by forwarding packets randomly. 
Instead a method called "full echo" was introduced.
In this method, a node requests to its neighbors when it has to make a forwarding decision.
The neighbors then return a number that characterizes the neighbors estimate of the time the packet would need reaching the destination.
Using this kind of information exchange shows clearly the similarities to the Bellman-Ford algorithm.
Their results indicate that an algorithm based on Q-learning performs equally well under low load and even better than shortest path algorithms under high load.\\
Choi and Yeung in \cite{RL_better_Q_adaptive} or Peshkin and Savova in \cite{RL_better_Q_adaptive_2} provide further developments of Q-routing to improve its performance under high and low load.
The trade-off is the short time of inefficiency when the agents learn the optimal behaviour, so how to forward packets in the most beneficial way.
One drawback is the usage of local information and the dependence on integrity of the other nodes.\\
Global knowledge about the network metrics can gained by using an \acrshort{sdn} controller.
It follows a review over approaches to use the available monitoring and control capabilities for more effective routing and \acrlong{te}.

\subsubsection{Based on Supervised Learning}
One strength of \acrlong{ml} is its ability to predict possible future states using the current information base.
The approach by Azzouni \textit{el al.} called Neuroute~\cite{neuroute} uses this advantage in their framework for dynamic routing in \acrshort{sdn} networks.
The system is divided into three parts, an estimator for the traffic matrix, a traffic matrix predictor and the so called traffic routing unit.
The estimator is based on OpenMeasure~\cite{openMeasure}, which aims to retrieve the current network state in an efficient way by learning to optimize the placement of the measurement resources in the network.
Based on the measurement, a neural network is trained to save the computed paths.
Doing so, routing configuration can be saved in a match to the current network state.\\
A similar approach is presented in \cite{ai_sl_meta_layer}. 
It introduces a supervised learning framework which is trained with previously computed paths showing like Neuroute that neural networks are capable to match routing decisions to network states.
But the quality of a trained network depends strongly on the variety of the input data and the presented solutions are not capable to learn the behaviour of an environment on their own.

\subsubsection{Based on Reinforcement Learning}
\acrlong{rl}, on the other hand learns through interaction with the environment and consequently does not require any dedicated training data.
In \cite{LB_RL}, Hu and Chen added to each network node a supervised reinforcement learning agent~\cite{supervised_rl}, which has the objective to balance the arriving traffic to prevent busy links.
Supervised \acrshort{rl} relates to the idea to speed up the learning by adding an additional entity which influences the agent additionally to the environment.
As reward function, the distance to the destination in Hops and the relative load in the next link is chosen. 
A \acrshort{rl} approach is used by Chavula \textit{et al.} in \cite{rl_ubuntu} to optimize UbuntuNet, an education network in Africa.
Each switch is seen as a state  $s$ with each connected neighbour switch as next state $s'$. 
The reward is calculated from a function of delay, capacity and number of flows between combinations of two switches.
Additionally, the system was tested using multiple paths per flow with the Q-values as splitting ratios.
The evaluation was not compared to other routing approaches and using the number of flows as a parameter for the reward calculation is not practical.\\
Xu \textit{et al.} propose in \cite{DRL_nw_experience_driven_networking} a data driven approach based on Deep Reinforcement Learning.
As states, the performance parameters throughput and delay are chosen.
The actions are split rations of the flows per link.
They optimize for network utility maximization in a combination with $\alpha$-fairness~\cite[p. 13 - 17]{mathematics_of_Internet_congestion_control} that is another model of the fairness in a network in which $\alpha$ is used as a trade-off between efficiency and fairness. 
Used for solving the problem, a mathematical framework called Gurobi Optimizer\footnote{Gurobi Optimizer, https://www.gurobi.com} were used.
There are several things to consider.
The approach does not solve the optimization problem on its own and uses mathematical programming for solving the network utility maximization problem.
It takes the task of mapping calculated solutions for the problem to specific network states.
Additionally, splitting paths can lead to better solutions, but a practical implementation like e.g. Multipath TCP~\cite{tcp_multipath} can be complicated in real life scenarios.
It delivers comprehensive results in simulated environments, such as the used packet simulator ns-3~\cite{ns3}, but the implementation would be more complicated in a real setup (i.e. hardware).\\
The focus of \cite{RL_multiple_controllers} lies on optimizing the routing for \acrshort{sdn} networks in a multi-layer hierarchical manner.
It attempts to provide a solution for real networks such as the Internet, which are divided into individual \acrlong{as}s, as described in section \ref{section:intra_inter}.
Therefore, the control plane is divided into three different layers, one for the so called Super Controller, followed by Domain Controllers which retrieve the network metrics by Slave Controller that are placed in the third layer.
Inside of a \acrshort{as} (i.e. Interdomain), the Domain Controller is responsible for the routing.
If the communication takes place between or via different \acrshort{as}s, the Super controller is consulted, which will decides about the global forwarding direction.
When a route for a new flow is demanded, the routing is calculated on a hop per hop basis.
Each switch is defined as a state.
The forwarding decision, in other words to which neighboring switch the packet should be sent, is defined as the action.
For the action selection, the softmax policy, as described in section \ref{section:exploration}, is used and SARSA as learning algorithm.
The reward function consists of different \acrshort{qos} aware functions: the packet loss, available bandwidth, Queuing delay in the current switch and the summed up transmission delay (i.e. the link latencies) till the destination.
The influences of the individual components can be weighted by varying factors.
In the evaluation, the approach is compared to Q-routing~\cite{RL_boyan}, which is very similar.
Like Q-routing, the algorithm does not use all the knowledge available to the controllers through the \acrshort{sdn} architecture.
Additionally, the forwarding packet per packet, is not suitable to realistic networks.
There is no guarantee that packets arrive in order, what can result in a collapsing congestion window for TCP~\cite{tcp_survey}. 
To compute the forwarding decision for each packet could overload the switches and congestion if the switch-controller response delay is included.\\
In \cite{rl_qos_leanring}, Al-Jawad \textit{et al.} propose the \acrshort{rl}-based framework LearnQoS.
It attempts to learn a policy that optimizes according to the \acrshort{qos} requirements.
As the state, the current traffic matrix is defined.
The traffic in the links is measured by the \acrshort{sdn} controller by requesting the switches for statistical information. 
Additionally, information about the throughput of the flows, the delay and packet losses are gathered.
These metrics are then used to check if the required bandwidth is met for the video streams, what also defines the reward.
The actions are defined as followed: the rates of best effort flows (i.e. the ones with the lowest priority) can be increased or reduced, best effort flows can be rerouted or not executing any change. 
Tabular Q-learning was selected as \acrshort{rl} algorithm.
For the evaluation, HTTP traffic were classified as best effort and video traffic as prioritized.
LearnQoS is evaluated in an emulated network with the Floodlight controller against shortest path routing and shows the potential of gaining stable throughput for prioritized flows by using \acrshort{rl} based \acrlong{te}.
In their action space, the rerouting is not specifically defined and as a result, it can interfere again with other video streams.
A simpler and common solution for prioritizing traffic would be to classify traffic and prioritize it, for example by using Differentiated services~\cite{rfc_diffserv}. \\
% Tabular Q-leanring -> wie diskretisisert???
An interesting approach is AuTo~\cite{drl_auto} by Li \textit{et al.} for traffic optimizing inner-datacenter networks.
Even if it refers to a different use case than a WAN or inter-datacenter network like Google's B4, their work deals with a realistic problem that short lived flows often are already gone before traffic optimizing decision could have been made.
First the state space was defined as a combination of all active and finished flows, the action was defined as their queuing priority and the reward was calculated as a ratio of the average throughput of all completed flows in two consecutive time steps and thus indicates how good a decision was with regard to the overall performance.
Using this system, the underlying problem was identified that the processing delay of existing frameworks for Deep \acrlong{rl} is too high to satisfy data center related traffic.
As solution, two different approaches have been introduced in which the flows were categorized and queued in an effective way.
An assumption has been made, called \textit{big-switch}, which means that the network is assumed to be non-blocking, non-congested and suitable load balanced.
As a result, in this work, routing and traffic engineering is ignored and the focus lies on the scheduling of flows.
The interesting parts are that optimization of networks can be complicated especially in real ones due to changing traffic patterns and short lived flows which can affect running optimization schemes even if they are already gone.\\
In \cite{drl_tide_china}, Sun \textit{et al.} proposed TIDE, a \acrshort{drl}-based routing system. It has the objective to determine the link weights of the network-graph under consideration of different \acrshort{qos} criterias.
The Floyd–Warshall algorithm~\cite[p. 693-699]{RL_DP_ALGO} is applied on this graph to determine the shortest path.
Yu \textit{et al.} provide in \cite{drl_drom} delivers a similar approach like TIDE with regard to the determination of link weights and then applying a shortest path algorithm.
In addition, the survey of \cite{ml_networks_survey} describes the utilization of \acrshort{ml} in networking.
Especially for applying \acrshort{ml} methods to \acrshort{sdn}, \cite{ml_sdn_survey}  and \cite{AI_SDN_survey} provide a comprehensive survey.

\section{Conclusion}
The presented approaches focus on an efficient routing of traffic flows in networks.
The efficiency can be defined by different optimization goals and by the constraints which are given. For example, the minimum latency or a maximum throughput, which should be achieved without overloading the links in the network.
Furthermore, it can be distinguished by the point in time when the decisions have to be made.
Some methods deal with routes of a newly emerging connection, while others deal with the subsequent rerouting of flows to achieve a specific optimization objective.
The routing of flows based on the \acrlong{spf} paradigm, even with constraints such as \acrshort{csp} or \acrshort{mcsp} do not take the influence of their routing decision on the rest of the network into account.
Especially flows which require a large bandwidth can negatively influence the network.
\acrlong{te} approaches are based on solving complicated multi-constrained problem formulations or models which do not scale well, often only focus on one optimization objective, and require additional engineering efforts.\\
This is where machine learning methods come into play, which, in addition to their foresighted character, are also capable of solving large optimization problems.
However, the presented \acrshort{ml} approaches do not offer a complete solution for routing.
Instead, they each solve parts while at the same time disregarding certain other aspects of the whole problem.
For instance, Q-routing~\cite{RL_boyan} does not use the global knowledge of an \acrshort{sdn} controller and additionally is not feasible for today's networks speeds.
Others, such as~\cite{DRL_nw_experience_driven_networking}, assume the arbitrary splitting of traffic, which is not feasible in reality.
The approaches~\cite{drl_tide_china} and \cite{drl_drom} generate a weighted graph on which shortest path routing algorithms are performed.
This means that they still rely on classic routing algorithms and only modify their behaviour by changing the graphs.
This is due to the application of \acrshort{drl} methods such as Deep Deterministic Policy Gradient~\cite{ddpg}, which output continuous actions.
For example AuTo~\cite{drl_auto} adds the big switch assumption and therefore simplifies the network behaviour.\\
In short, the current approaches based on \acrlong{ml} do not provide a complete solution for routing or traffic engineering by unrealistic assumptions or restriction to partial problems.


% Using a central controller for the whole network allows to use broader reaching information about the network state.

% maybe.....
%\cite{rl_for_call_admission} first occurance, laying routes \\ 

