## RL_for_routing

Repository for Diploma thesis of Peter Sossalla

run:
sudo python Run_Measurement.py <options>

(sudo is necessary for mininet)

<options>:

--mn_path "Path to mininet file"

##### config file: Controller/config.cfg
The program is controlled from the values set in the config file. No necessary 

#### Folder description
/Controller:
includes the controller implementation

/Mininet:
includes the Mininet emulation scripts with different topologies

/TryOuts:
different script to visualize or test important parts of the program

#### Beleg
The Thesis:
https://tex.zih.tu-dresden.de/read/xfskdtbdsbnh

Mininet Emulation:
python Four_switches_two_ways.py

Controller (Routing via RL):
ryu-manager remote_controller_RL.py

#### Possible extensions:
adding throughput to reward, mixed reward function (port stats, flow stats)
also non-static flows like TCP
neural network instead of tabular
joining/leaving flows
actions gruppieren