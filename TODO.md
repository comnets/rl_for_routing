###  Präsentation:
* Weniger technische Details
* Mehr Motivation
* grouping nennen
* Warum nice? => Model free!!! (wird nicht benötigt)

### Messungen:
* load verändern vs. statisch
* Zeit bis Konvergenz:
    * Topologieabhängig? Wovon abhängig? different traffic?
    * korrekte Begriffe: Einheiten, Latencies
* vll. skalierbarkeit?
* Initialisieren + Änderung der load-level
* Direkt bei 4 angangen oder von 3 zu vier starten -> wie verhält sich das?
* Learning Rate variieren

### Fragen zu beantworten:
* Combining flows as sw2sw connection useful
* how fast converge?
* adding bw (how)?
* Zu L_(h1_h2) ändern
    * wie action/state space sinnvoller definieren
    * Q-Learning? -> nur wenn man iwie erkennt dass zum bsp. nicht umrouten sollte in dem jeweiligen Zustand
    * Multi armed bandit -> wenn es keinen delayed reward gibt (in my case: yes)
    
### Sequence Plots:
1. Plot SPF vs. RL Routing
2. Average Reward to Steps
3. Flow Adding -> how (fast) reacts
4. Convergence time to topology/number of hosts
5. Learning rate (alpha)/ Exploration (epsilon) varying