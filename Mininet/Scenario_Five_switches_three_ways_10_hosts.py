from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from thread import start_new_thread
import os, stat
import json
import time
import csv
import requests
import sys
sys.path.append("...")
sys.path.append("..")
sys.path.append(".")

from Controller.config import Config


#             s2
#  h11   10ms/     \10ms    h41
#     -- s1    -----      s4 --
#  h12    14ms\     /14ms   h42
#             s3

####################################################################
############### Scenario - 10h 5 sw    #############################
####################################################################

def reset_load_level(loadLevel):
    requests.put('http://0.0.0.0:8080/simpleswitch/params/load_level', data=json.dumps({"load_level": loadLevel}))
    requests.put('http://0.0.0.0:8080/simpleswitch/params/reset_flag', data=json.dumps({"reset_flag": True}))

def reset_iteration(iteration):
    requests.put('http://0.0.0.0:8080/simpleswitch/params/iteration', data=json.dumps({"iteration": iteration}))
    requests.put('http://0.0.0.0:8080/simpleswitch/params/iteration_flag', data=json.dumps({"iteration_flag": True}))

def stop_controller():
    requests.put('http://0.0.0.0:8080/simpleswitch/params/stop_flag', data=json.dumps({"stop_flag": True}))

def startIperf(host1, host2, amount, port, timeTotal, loadLevel):
    pkt_size = Config.size_iperf_pkt_bytes
    #host2.cmd("iperf -s -u -p {} &".format(port))
    bw = float(amount) * (float(loadLevel) / float(10))
    print("Host {} to Host {} Bw: {}".format(host1.name, host2.name, bw))
    command = "iperf -c {} -u -p {} -t {} -b {}M -l {} &".format(host2.IP(), port, timeTotal, bw, pkt_size)
    host1.cmd(command)

def write_in_File(fileName, logs, loadlevel, iteration_split_up_flag, iteration):
    dir = logs + '/'
    if iteration_split_up_flag:
        dir = dir + str(iteration) + '/'
    with open('{}{}.csv'.format(dir, fileName), 'a') as csvfile:
        fileWriter = csv.writer(csvfile, delimiter=',')
        fileWriter.writerow([loadlevel, time.time()])

def clearingSaveFile(fileName, logs):
    dir = logs + '/'
    with open('{}{}.csv'.format(dir, fileName), 'w') as file:
        file.write("# loadlevel, timestamp \n")

def clearingSaveFileIterations(fileName, logs, iterations):
    for iteration in range(iterations):
        dir = logs + '/' + str(iteration) + '/'
        if not os.path.exists(dir):
            os.makedirs(dir)
            # give folder rights
            os.chmod(dir, stat.S_IRWXO)
        with open('{}{}.csv'.format(dir, fileName), 'w') as file:
            file.write("# loadlevel, timestamp \n")

def minToSec(min):
    return min * 60

def four_switches_network():
    net = Mininet(topo=None,
                  build=False,
                  ipBase='10.0.0.0/8', link=TCLink)

    queue_lenght = Config.queue_lenght

    bw_max_dict = Config.bw_max_dict

    # linkarray
    linkArray = []
    splitUpLoadLevelsFlag = Config.splitUpLoadLevelsFlag
    logs = Config.log_path
    # importante! the load levels for measurements
    loadLevels = Config.load_levels
    print("LoadLevel: {}".format(loadLevels))
    timeTotal = minToSec(Config.duration_iperf_per_load_level_minutes)
    controllerIP = '127.0.0.1'
    fileName = 'timestamp_changing_load_levels_mininet'
    info('*** Adding controller\n')
    c0 = net.addController(name='c0',
                           controller=RemoteController,
                           ip=controllerIP,
                           protocol='tcp',
                           port=6633)

    info('*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s21 = net.addSwitch('s21', cls=OVSKernelSwitch)
    s22 = net.addSwitch('s22', cls=OVSKernelSwitch)
    s23 = net.addSwitch('s23', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)


    info( '*** Add hosts\n')
    h11 = net.addHost('h11', cls=Host, ip='10.0.0.11', defaultRoute=None)
    h12 = net.addHost('h12', cls=Host, ip='10.0.0.12', defaultRoute=None)
    h13 = net.addHost('h13', cls=Host, ip='10.0.0.13', defaultRoute=None)
    h14 = net.addHost('h14', cls=Host, ip='10.0.0.14', defaultRoute=None)
    h15 = net.addHost('h15', cls=Host, ip='10.0.0.15', defaultRoute=None)

    h41 = net.addHost('h41', cls=Host, ip='10.0.0.41', defaultRoute=None)
    h42 = net.addHost('h42', cls=Host, ip='10.0.0.42', defaultRoute=None)
    h43 = net.addHost('h43', cls=Host, ip='10.0.0.43', defaultRoute=None)
    h44 = net.addHost('h44', cls=Host, ip='10.0.0.44', defaultRoute=None)
    h45 = net.addHost('h45', cls=Host, ip='10.0.0.45', defaultRoute=None)

    info('*** Add links\n')
    linkArray.append(net.addLink(s1, s21, delay='5ms', bw=6, max_queue_size=queue_lenght))
    linkArray.append(net.addLink(s1, s22, delay='7ms', bw=4, max_queue_size=queue_lenght))
    linkArray.append(net.addLink(s1, s23, delay='9ms', bw=10, max_queue_size=queue_lenght))
    linkArray.append(net.addLink(s21, s3, delay='5ms', bw=6, max_queue_size=queue_lenght))
    linkArray.append(net.addLink(s22, s3, delay='7ms', bw=4, max_queue_size=queue_lenght))
    linkArray.append(net.addLink(s23, s3, delay='9ms', bw=10, max_queue_size=queue_lenght))

    net.addLink(h11, s1)
    net.addLink(h12, s1)
    net.addLink(h13, s1)
    net.addLink(h14, s1)
    net.addLink(h15, s1)

    net.addLink(h41, s3)
    net.addLink(h42, s3)
    net.addLink(h43, s3)
    net.addLink(h44, s3)
    net.addLink(h45, s3)

    info('*** Starting network\n')
    net.build()
    info('*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info('*** Starting switches\n')
    net.get('s1').start([c0])
    net.get('s21').start([c0])
    net.get('s22').start([c0])
    net.get('s23').start([c0])
    net.get('s3').start([c0])



    iterations = Config.iterations
    if iterations > 1:
        iteration_split_up_flag = True
    else:
        iteration_split_up_flag = False

    # erasing previous file
    if not splitUpLoadLevelsFlag:
        if iteration_split_up_flag:
            clearingSaveFileIterations(fileName, logs, iterations)
        else:
            clearingSaveFile(fileName, logs)

    time.sleep(15)
    # incrementing the load
    for iteration in range(iterations):
        i = 0
        clearingSaveFileIterations(fileName, logs, iterations)
        for loadLevel in loadLevels:
            # iperf threads
            # if the load levels are not split up -> write the load level change
            if splitUpLoadLevelsFlag:
                reset_load_level(loadLevel)
            if not splitUpLoadLevelsFlag:
                write_in_File(fileName, logs, loadLevel, iteration_split_up_flag, iteration)
            # send load level
            print("(Re)starting iperf -- loadLevel:  {}".format(loadLevel))
            start_new_thread(startIperf, (h11, h41, 2.75, 5001, timeTotal, loadLevel))
            time.sleep(0.2)
            start_new_thread(startIperf, (h12, h42, 2.75, 5001, timeTotal, loadLevel))
            time.sleep(0.2)
            start_new_thread(startIperf, (h13, h43, 3.75, 5001, timeTotal, loadLevel))
            time.sleep(0.2)
            start_new_thread(startIperf, (h14, h44, 4.75, 5001, timeTotal, loadLevel))
            time.sleep(0.2)
            start_new_thread(startIperf, (h15, h45, 4.75, 5001, timeTotal, loadLevel))
            i = i + 1
            time.sleep(timeTotal)

        # last load level past
        if not splitUpLoadLevelsFlag:
            #if iteration < iterations:
                #reset_load_level(-1)
            write_in_File(fileName, logs, -1, iteration_split_up_flag, iteration)
        if iteration_split_up_flag and iteration < iterations - 1:
            reset_iteration(iteration + 1)
    stop_controller()
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')

four_switches_network()
