from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from thread import start_new_thread
import time
import csv
import random
import json
#             s2
#       10ms/     \10ms
#   h1 -- s1        s4 -- h2
#         14ms\     /14ms
#             s3

def startIperf(host1, host2, amount, port, timeTotal):
    host2.cmd("iperf -s -u -p {} &".format(port))
    bw = float(amount)
    print("Host {} to Host {} Bw: {}".format(host1.name, host2.name, bw))
    command = "iperf -c {} -u -p {} -t {} -b {}M &".format(host2.IP(), port, timeTotal, bw)
    host1.cmd(command)

def write_in_File(fileName, loadlevel):
    with open('../../reward_mininet.csv', 'a') as csvfile:
        fileWriter = csv.writer(csvfile, delimiter=',')
        fileWriter.writerow([-1, json.dumps(loadlevel), time.time()])

def clearingTheSaveFile():
    open('../../reward_mininet.csv', 'w').close()

# contraints
# n = 3
# sum flows < 17
# each flow < 10
def calcFlowBwLevels():
    while True:
        flowList = [(random.random() * 10.0) for _ in range(3)]
        #r1, r2, r3 = random.random() * 10.0,random.random() * 10.0,random.random() * 10.0
        if sum(flowList) < 17:
            #flowList=[r1, r2 ,r3]
            break
    return flowList

def four_switches_network():
    net = Mininet(topo=None,
                  build=False,
                  ipBase='10.0.0.0/8', link=TCLink)
    # linkarray
    linkArray = []

    #controllerIP = '192.168.56.129'
    controllerIP = '127.0.0.1'

    info('*** Adding controller\n')
    c0 = net.addController(name='c0',
                           controller=RemoteController,
                           ip=controllerIP,
                           protocol='tcp',
                           port=6633)

    info('*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch)

    info( '*** Add hosts\n')
    h11 = net.addHost('h11', cls=Host, ip='10.0.0.11', defaultRoute=None)
    h12 = net.addHost('h12', cls=Host, ip='10.0.0.12', defaultRoute=None)
    h13 = net.addHost('h13', cls=Host, ip='10.0.0.13', defaultRoute=None)

    h41 = net.addHost('h41', cls=Host, ip='10.0.0.41', defaultRoute=None)
    h42 = net.addHost('h42', cls=Host, ip='10.0.0.42', defaultRoute=None)
    h43 = net.addHost('h43', cls=Host, ip='10.0.0.43', defaultRoute=None)

    info('*** Add links\n')
    linkArray.append(net.addLink(s1, s2, delay='10ms', bw=10, max_queue_size=500))
    linkArray.append(net.addLink(s2, s4, delay='10ms', bw=10, max_queue_size=500))
    linkArray.append(net.addLink(s1, s3, delay='14ms', bw=10, max_queue_size=500))
    linkArray.append(net.addLink(s3, s4, delay='14ms', bw=10, max_queue_size=500))

    net.addLink(h11, s1)
    net.addLink(h12, s1)
    net.addLink(h13, s1)

    net.addLink(h41, s4)
    net.addLink(h42, s4)
    net.addLink(h43, s4)

    info('*** Starting network\n')
    net.build()
    info('*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info('*** Starting switches\n')
    net.get('s1').start([c0])
    net.get('s2').start([c0])
    net.get('s3').start([c0])
    net.get('s4').start([c0])

    # erasing previous file
    clearingTheSaveFile()

    # how many iterations
    iterations = 50
    # how long in minutes
    time_min = 30
    time_s = time_min * 60
    i = 0
    while i < iterations:
    # iperf threads
        direction_1_to_4 = calcFlowBwLevels()
        direction_4_to_1 = calcFlowBwLevels()
        write_in_File('Reward_Mininet', [direction_1_to_4, direction_4_to_1])
        time.sleep(15)
        start_new_thread(startIperf, (h11, h41, direction_1_to_4[0], 5001, time_s))
        time.sleep(0.2)
        start_new_thread(startIperf, (h12, h42, direction_1_to_4[1], 5001, time_s))
        time.sleep(0.2)
        start_new_thread(startIperf, (h13, h43, direction_1_to_4[2], 5001, time_s))
        time.sleep(0.2)
        start_new_thread(startIperf, (h41, h11, direction_4_to_1[0], 5002, time_s))
        time.sleep(0.2)
        start_new_thread(startIperf, (h42, h12, direction_4_to_1[1], 5002, time_s))
        time.sleep(0.2)
        start_new_thread(startIperf, (h43, h13, direction_4_to_1[2], 5002, time_s))
        time.sleep(time_s+3)
        print("restarting iperf -- loadLevels - 1-4: {} 4-1: {}".format(direction_1_to_4, direction_4_to_1))
        i = i + 1
    # end
    write_in_File('Reward_Mininet', -1)
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )

four_switches_network()