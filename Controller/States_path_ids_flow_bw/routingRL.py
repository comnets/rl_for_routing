import random
import copy
import time
import matplotlib.pyplot as plt
from collections import defaultdict

def greedy_q_routing(n, startNode, endNode, T, Q, alpha, epsilon):
    plotArray = []
    nodes = [0,0]
   # ax = {}
    t0 = time.time()
    for i in range (n):
        stateNow = startNode
        # target = endNode
        reached_target = False
        while not reached_target:
            possibleMoves= list(Q[stateNow].keys())
            # check if only one possible
            if len(possibleMoves) < 2:
                nextState = possibleMoves[0]
            else:
                # get the best choice, or better choose from the best choices
                bestAction = random.choice([min(Q[stateNow], key=Q[stateNow].get)])
                #get random number between 0-1 and check with the epsilon (epsilongreedy)
                if(random.random() < epsilon):
                    # kick out best from valid choices
                    possibleMoves.pop(possibleMoves.index(bestAction))
                    # choose randomly from the rest
                    nextState = random.choice(possibleMoves)
                else:
                    nextState = bestAction
            Q = calc_new_Q(stateNow, nextState, alpha, T, Q)
            stateNow = nextState
            # check if target reached, end node can be more than one node
            if nextState in endNode:
                reached_target = True
        plotArray.append({i:copy.deepcopy(Q)})
    x = []
    y1_2 = []
    y1_3 = []
    #print(plotArray)
    for e in range(len(plotArray)):
        xx =  list(plotArray[e].keys())[0]
        yy1_1 = plotArray[e][xx][1][2]
        x.append(xx)
        yy1_2 = plotArray[e][xx][1][3]
        y1_2.append(yy1_1)
        y1_3.append(yy1_2)
    #plt.plot(x,y1_2)
    #plt.plot(x,y1_3)
    #plt.show()
    t1 = time.time()
    print("Time for calc Q Functions:" + str((t1-t0)*1000) + " ms")
    # get bestRoute
    bestRoute = getBestRoute(Q, startNode, endNode, 10)
    return bestRoute

def calc_new_Q(stateNow, nextState, alpha, T, Q):
    t = T[stateNow][nextState]
    q = Q[stateNow][nextState]
    updated_q = q + alpha * (t + min(Q[nextState].values()) - q)
    Q[stateNow][nextState] = updated_q
    return Q
# get the best possible way

def getBestRoute(Q, startNode, endNode, maxHops):
    route = [startNode]
    # if last element not in end node, do not stop
    while route[-1] not in endNode:
        # check for minimum-value of all possible hops
        a = Q[route[-1]]
        nextHop = min(a, key=a.get)#,key=Q[route[-1]].get)
        route.append(nextHop)
        # prevent loop!!
        if len(route)>2 and route[-1] in route[:-1]:
            break
    return route
