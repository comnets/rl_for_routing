from enum import Enum
import time
import os
import copy
import math
import itertools as it
import numpy as np
import json
import random
# for communication between mininet and ryu
import zmq
import random
import sys
import csv

# different possible modes
class QMode(Enum):
    MULTI_ARMED_BANDIT_NO_WEIGHT = 0
    MULTI_ARMED_BANDIT_WITH_WEIGHT = 1
    Q_LEARNING = 2

#### program control variables
# how many rewards are gathred before considering taking a new action
REWARDS_BEFORE_ACTION_CHOOSE = 3
# how long to wait until starting to gather new rewards
DELAYED_REWARD_COUNTER = 3
MAX_LENGHT_DIFFSET = 2
MAX_PAST_REWARDS = 5
# modes:
MODE = QMode.Q_LEARNING

def f(connection, ):
    print('process id:', os.getpid())
    ##### RL Parameters:
    alpha = 0.9
    gamma = 0.3
    epsilon = calc_epsilon(1)
    generalIterator = 0
    delayedRewardCounter = 0
    tempFlows = []

    ##### Tables
    Q = []

    #### Bw division
    levelList = [3, 6, 10]
    # tolerance level of hysteresis
    tolerance = 0.3

    # fill-up-arrays / dicts
    tempSavedRewards = []
    tempSavedRewardBeforeSaving = []
    savingValueArray = []
    previousState = {}
    previousAction = {}

    combinationsMatchingTable = {}

    savingIterator = 0
    HOW_MANY_TIMES_UNTIL_SAVE_REWARD = 10
    HOW_MANY_TIMES_UNTIL_SAVE_Q = 50
    # list of (avgReward, combination)
    savedQPerfectPath = []

    # clean up the save file
    clearingSaveFile('reward_controller')
    clearingSaveFile('Q_array')
    clearingSaveFile('Q_array_best')
    # maximum time
    maxMinutes = 1600 #425
    startingTime = time.time()
    while True:
        #time.sleep(0.1)
        # TODO: take care that recv() BLOCKS the thread
        elements = connection.recv()
        if (len(elements) > 0):
            if (len(elements[0]) > 0):
                paths_per_flow = elements[1]
                latencydict = elements[2]
                bwDict = elements[3]
                currentState = getCurrentCombinations(elements[0], bwDict, previousState, levelList, tolerance)

                # first time a flow joins -> creates a q-table
                if len(tempFlows) < 1:
                    copied_paths_per_flow = copy.deepcopy(paths_per_flow)
                    Q, actions, stateTransitions = update_Q_table_path_joined(Q, copied_paths_per_flow, levelList)
                    currentState = getCurrentCombinations(elements[0], bwDict, previousState, levelList, tolerance)
                    print("CHOSENPATH: {}".format(currentState))
                    tempFlows = list(currentState.keys())
                else:
                    # print("Element Keys: {}".format(elements[0].keys()))
                    # new flow added -> update best route
                    setTempFlows = set(tempFlows)
                    setChosenPaths = set(list(currentState.keys()))

                    # if flows were added/deleted etc
                    if abs(len(setChosenPaths) - len(setTempFlows)) > 0:
                        copied_paths_per_flow = copy.deepcopy(paths_per_flow)
                        # pointer to combinations
                        differenceSet = setChosenPaths.difference(setTempFlows)
                        Q, actions, stateTransitions = update_Q_table_path_joined(Q, copied_paths_per_flow,
                                                                                  levelList,
                                                                                  differenceSet)
                        # reset the previous state
                        previousState = {}
                        currentState = getCurrentCombinations(elements[0], bwDict, previousState, levelList, tolerance)
                        tempFlows = list(currentState.keys())
                        tempSavedRewards.clear()
                    rewardsqroot, rewardList = calculateRewards(currentState, latencydict)
                    if((delayedRewardCounter >= DELAYED_REWARD_COUNTER)):
                        tempSavedRewards.append(rewardsqroot)
                        tempSavedRewardBeforeSaving.append(rewardsqroot)
                    if ((generalIterator % 100) < 1):
                        epsilon = calc_epsilon(generalIterator)
                        print("-------number of batch: {} calced epsilon: {}".format(epsilon, generalIterator))
                    if not (len(tempSavedRewards) < REWARDS_BEFORE_ACTION_CHOOSE):
                        # calc qValuePrev
                        if len(previousState) > 0 and len(previousAction) > 0:
                            if (MODE ==  QMode.MULTI_ARMED_BANDIT_NO_WEIGHT or MODE == QMode.MULTI_ARMED_BANDIT_WITH_WEIGHT):
                                Q = calc_new_Q_bandit(previousState, currentState, alpha, gamma, copy.deepcopy(Q),
                                           np.mean(tempSavedRewards), previousAction)
                            elif (MODE == QMode.Q_LEARNING):
                                Q = calc_new_Q_QL(previousState, currentState, alpha, gamma, copy.deepcopy(Q),
                                                      np.mean(tempSavedRewards), previousAction)
                        # choose action
                        action = json.loads(choose_action(currentState, Q, epsilon))
                        # do the action (if it is a transition) (send it into the pipe):
                        if "_" in action[0]:
                            connection.send(action)
                        previousAction = copy.deepcopy(action)
                        previousState = copy.deepcopy(currentState)
                        # find out next state:
                        currentState = getNextState(stateTransitions, currentState, action)
                        print("NextState: {}".format(currentState))
                        delayedRewardCounter = 0
                        tempSavedRewards.clear()
                    delayedRewardCounter += 1

                # savingIterator
                if ( not (savingIterator % HOW_MANY_TIMES_UNTIL_SAVE_REWARD) and savingIterator > 0):
                    saveRewardCsv('reward_controller', np.mean(tempSavedRewardBeforeSaving), savingIterator // REWARDS_BEFORE_ACTION_CHOOSE)
                    # saving values of perfect path
                    #perfectPath = '{"10.0.0.11_10.0.0.41": [1, 3, 4], "10.0.0.12_10.0.0.42": [1, 3, 4], "10.0.0.13_10.0.0.43": [1, 2, 4], "10.0.0.41_10.0.0.11": [4, 3, 1], "10.0.0.42_10.0.0.12": [4, 3, 1], "10.0.0.43_10.0.0.13": [4, 2, 1]}'
                    #savedQPerfectPath.append([time.time(), Q[perfectPath]])
                    tempSavedRewardBeforeSaving.clear()

                if( not(savingIterator % HOW_MANY_TIMES_UNTIL_SAVE_Q) and savingIterator > 0):
                    # Q , savingIterator, averageReward
                    savingValueArray.append((copy.deepcopy(Q), savingIterator // REWARDS_BEFORE_ACTION_CHOOSE,
                                             np.mean(tempSavedRewardBeforeSaving)))
                    saveQCurrent(Q)
                savingIterator = savingIterator + 1
                generalIterator = generalIterator + 1

                # check if exit -> time.time are seconds
                if int((time.time() - startingTime) / 60) > maxMinutes:
                    #saveQ(savingValueArray)
                    #saveQBest(savedQPerfectPath)
                    print("Exited after {} episodes".format(generalIterator))
                    break

def getNextState(stateTransitions, currentState, action):
    nextState = {}
    actionTuple = tuple(action)
    if(actionTuple[0] == 'NoTrans'):
        return currentState
    for stateTrans in stateTransitions:
        if stateTrans[0] == currentState and stateTrans[1] == actionTuple:
            nextState = stateTrans[2]
    return nextState

def calculateRewards(chosenPaths, latencyDict):
    rewardList = []
    rewardListJustValues = []
    sqrootLatency = 0
    for path in chosenPaths:
        cost = get_path_cost(latencyDict, chosenPaths[path][0])
        rewardList.append((path, cost))
        rewardListJustValues.append(cost)
    costBefore = 0
    for element in rewardListJustValues:
        costBefore += element ** 2
    sqrootLatency = math.sqrt(costBefore)
    return -sqrootLatency, rewardList

def update_Q_table_path_joined(prevQ, paths_per_flow,  levelList, diffSet={}):
    t0 = time.time()
    paths_per_flow_copied = copy.deepcopy(paths_per_flow)
    paths_per_flow_filtered = paths_per_flow_copied
    #filterStateSpacesByHOPS(paths_per_flow_copied, paths_per_flow, 8)
    new_states = getPossibleStates(copy.deepcopy(paths_per_flow_filtered), levelList)
    #print("states: {}".format(new_states))
    print("got possible states len: {}: {}".format(len(new_states), new_states))
    actions = getActionsForStates(new_states, paths_per_flow_filtered)
    print("got actions per states: {}".format(len(actions)))
    t01 = time.time()
    stateTransitions = getStateTransitions(actions)
    print("got state transitions: {} in t: {}s".format(len(stateTransitions), time.time() - t01))
    # matching
    # create Q table
    t02 = time.time()
    Q = createNewQTable(actions)
    print("created new Q table: {} in t: {}s".format(len(Q), time.time() - t02))
    #print("Q: {}".format(Q))
    if len(prevQ) > 0 and len(diffSet) and len(diffSet) == MAX_LENGHT_DIFFSET:
        Q = mergingQtable(prevQ, Q, diffSet)

    print("Time to merge: {} micro_sec".format((time.time() - t0) * 10 ** 6))
    print("Action Size: {}".format(len(actions)))
    print("Q size: {}".format(len(Q.keys())))
    return Q, actions, stateTransitions

# merging operation
def mergingQtable(oldQ, newQ, differenceSet):
    #print("OldQ: {}".format(oldQ))
    #print("NewQ: {}".format(newQ))
    newQCopy = copy.deepcopy(newQ)
    for state in newQ:
        for action in newQ[state]:
            actionId = action[0]
            if actionId not in differenceSet:
                # find the sate with the smallest difference
                dictStateStr = ''
                dictState = json.loads(state)
                # delete the flow IDs of difference
                for difference in differenceSet:
                    dictState.pop(difference)
                # necessary to find the constellation that matches -> just json.dump does not give deterministic order
                oldQKeysDict = list(oldQ.keys())
                for oldQComb in oldQKeysDict:  # NEED TO BUILD UP TUPLE
                    oldKeysSet = json.loads(oldQComb)
                    # other variant:
                    # oldKeysSet = list(json.loads(oldQComb).items())
                    # if len(oldKeysSet.difference(set(dictState.items()))) < 1 and len(set(dictState.keys()).difference(oldKeysSet)) < 1:
                    if oldKeysSet == dictState:
                        dictStateStr = oldQComb  # json.dumps(oldQComb)
                if (len(dictStateStr) > 0):
                    # clean up the action set
                    actionOld = json.loads(action)
                    if list(actionOld)[0] not in differenceSet:
                        newQCopy[state][action] = oldQ[dictStateStr][action]
    return newQCopy


# creates a new Q table based on the actions of a state constellation
def createNewQTable(actions):
    Q = {}
    for actionElement in actions:
        state = json.dumps(actionElement[0], sort_keys=True)
        action = json.dumps(actionElement[1], sort_keys=True)
        # flowId = action[0]
        # nextPath = action[1]
        if state not in Q:
            Q[state] = {}
        # steps
        Q[state][action] = [0, -math.inf, []]
    return Q


# find out matched items, no deep copy!!
def additionalStates(prevQ, stateTransitions):
    for stateTransition in stateTransitions:
        prevState = stateTransition[0]
        nextState = stateTransition[2]
        # check if in Q
        for currentCombination in prevQ:
            if prevState == currentCombination[0] and nextState == currentCombination[2]:
                stateTransitions.remove(stateTransition)
    return stateTransitions


def getStateTransitions(actions):
    stateTransitionPairs = []
    for action in actions:
        currentstate = action[0]
        id = action[1][0]
        nextPath = action[1][1]
        nextState = copy.deepcopy(currentstate)
        # change the state
        # change the state
        if 'NoTrans' not in id:
            nextState[id][0] = nextPath
        stateTransitionPairs.append((currentstate, action[1], nextState))
    return stateTransitionPairs

#          sw2
# h11     /    \    h41
# h12  sw1      sw4 h42
# h13     \    /    h43
#          sw3
# (3*3*2) * 2 * 3 = 108
# (who_to_who * both directions) * different routes * bw_levels

def getPossibleStates(paths_per_flow, bw_levels):

    t0 = time.time()
    # reduce action and state space
    # paths_per_flow = filterStateSpacesByHOPS(paths_per_flow, paths_per_flow, 1)
    # combinations = it.product(*(paths_per_flow[id] for id in allIds))
    #print("Path per flow: {}".format(paths_per_flow))
    #print("Path per flow item: {}".format(paths_per_flow.items()))
    modified_path_per_flow = {}
    for id in paths_per_flow:
        if id not in modified_path_per_flow:
            modified_path_per_flow[id] = []
        arrayPaths = paths_per_flow[id]
        for path in arrayPaths:
            for bw_level in bw_levels:
                modified_path_per_flow[id].append([path, bw_level])
    flat = [[(k, v) for v in vs] for k, vs in modified_path_per_flow.items()]
    print("Flat len: {}: {}".format(len(flat), flat))
    combinations = [dict(items) for items in it.product(*flat)]
    combinationsList = list(combinations)

    #flat2 = [[(k, v) for v in vs] for k, vs in paths_per_flow.items()]
    # print("Combinations: {}".format(combinationsList))
    print("calcLenght possibleStates with bwL n= {}: {} micro_sec".format(len(combinationsList), (time.time() - t0) * 6))
    return combinationsList


def filterStateSpacesByHOPS(paths_per_flow, chosen_paths, bound=1):
    print("PATHS PER FLOW: {}".format(paths_per_flow))
    for flowId in paths_per_flow:
        #print("Array: {}".format(paths_per_flow[flowId][0]))
        minimumlenght = min([len(x) for x in paths_per_flow[flowId]])
        for path in paths_per_flow[flowId]:
            if len(path) > minimumlenght + bound:
                # that is not the current chosen one
                if chosen_paths[flowId] != path:
                    paths_per_flow[flowId].remove(path)
    return paths_per_flow


def getActionsForStates(combinations, paths_per_flows):
    actions = []
    #print("Combination: {}".format(combinations))
    for combination in combinations:
        otherPaths = copy.deepcopy(paths_per_flows)
        for flowId in combination:
            # find out other combinations
            chosenPath = combination[flowId][0]
            # all the other paths
            for otherPathById in otherPaths[flowId]:
                # kick out same paths of combinations
                if (otherPathById == chosenPath):
                    otherPaths[flowId].remove(otherPathById)
            # now, build possible next actions
            for chosenPath in otherPaths[flowId]:
                actions.append((combination, (flowId, chosenPath)))
        actions.append((combination, ('NoTrans', [])))
    return actions


def getActionsPerCurrentState(chosenPaths, paths_per_flow):
    actions = []
    otherPaths = copy.deepcopy(paths_per_flow)
    for chosenPath in chosenPaths:
        idPath = chosenPath
        selectedPath = chosenPaths[idPath]
        # cleaning up so possible actions get clear
        for path in otherPaths[idPath]:  # is it idPath?
            #print("Path[0]: {}".format(path[0]))
            if path[0] == selectedPath:
                otherPaths[idPath].remove(path)
    ids = otherPaths.keys()
    for id in ids:
        for possiblePath in otherPaths[id]:
            actions.append((id, chosenPaths[id], possiblePath[0]))
    return actions


def getBestRoute(Q, startNode, endNode, maxHops):
    route = [startNode]
    # if last element not in end node, do not stop
    while route[-1] not in endNode:
        # check for minimum-value of all possible hops
        a = Q[route[-1]]
        nextHop = min(a, key=a.get)  # ,key=Q[route[-1]].get)
        route.append(nextHop)
        # prevent loop!!
        if len(route) > 2 and route[-1] in route[:-1]:
            break
    return route


# Q(s,a) <- Q(S,a) + alpha[R + gamma*max Q(S',a)-Q(S,a)]
# tracking a non stationary problem
def calc_new_Q_bandit(stateNow, nextState, alpha, gamma, Q, reward, action):
    # cambiamos
    stateNowStr = json.dumps(stateNow, sort_keys=True)
    nextStateStr = json.dumps(nextState, sort_keys=True)
    actionStr = json.dumps(action, sort_keys=True)
    keyMaxValue = keywithmaxActionval(Q[nextStateStr])
    try:
        # if chosen Q-value is set infinity -> necessary to change
        if math.isinf(Q[stateNowStr][actionStr][1]):
            Q[stateNowStr][actionStr][1] = 0
        # weighted average: Q_(n+1) = (1-alpha)^n*Q_1 + sum(i=1 -> n) alpha * (1 - alpha)^(n-i) R_i)
        if MODE == QMode.MULTI_ARMED_BANDIT_WITH_WEIGHT:
            lastRewards = Q[stateNowStr][actionStr][2]
            n = len(lastRewards)
            q_n = 0
            # last one is highest weighted
            for i in range(0, n, 1):
                q_n = q_n + alpha * (1 - alpha) ** (n - i) * lastRewards[i]
            Q[stateNowStr][actionStr][1] = q_n + alpha * (reward - q_n)
            # save the previous reward (list max elements)
            Q[stateNowStr][actionStr][2].append(reward)
            if (len(Q[stateNowStr][actionStr][2]) > MAX_PAST_REWARDS):
                Q[stateNowStr][actionStr][2].pop(0)
        # non weighted average; Q_n+1 = Q_n + 1/n * (R_n - Q_n)
        elif MODE == QMode.MULTI_ARMED_BANDIT_NO_WEIGHT:
            Q[stateNowStr][actionStr][1] = Q[stateNowStr][actionStr][1] + (1 / (Q[stateNowStr][actionStr][0])) * (
                        reward - Q[stateNowStr][actionStr][1])
        # total visits
        Q[stateNowStr][actionStr][0] = Q[stateNowStr][actionStr][0] + 1
    except KeyError:
        print("Q: {}".format(Q))
        print("StateNowStr: {}".format(stateNowStr))
        print("KEYERROR!")
    return Q

# has to constuct
def calc_new_Q_QL(stateNow, nextState, alpha, gamma, Q, reward, action):
    # cambiamos
    stateNowStr = json.dumps(stateNow, sort_keys=True)
    nextStateStr = json.dumps(nextState, sort_keys=True)
    actionStr = json.dumps(action, sort_keys=True)
    #actionStr = json.dumps(action)
    # Q[stateNow][action] = Q[stateNow][action] + alpha*(reward + gamma * max(Q[nextState]) - Q[stateNow][action])
    keyMaxValue = keywithmaxActionval(Q[nextStateStr])
    # if chosen Q-value is set infinity -> necessary to change
    if math.isinf(Q[stateNowStr][actionStr][1]):
        Q[stateNowStr][actionStr][1] = 0
    if math.isinf(Q[nextStateStr][keyMaxValue][1]):
        Q[nextStateStr][keyMaxValue][1] = 0
    try:

        Q[stateNowStr][actionStr][1] = Q[stateNowStr][actionStr][1] + alpha * (reward + gamma * Q[nextStateStr][keyMaxValue][1] - Q[stateNowStr][actionStr][1])
        Q[stateNowStr][actionStr][0] = Q[stateNowStr][actionStr][0] + 1
    except KeyError:
        print("Q: {}".format(Q))
        print("StateNowStr: {}".format(stateNowStr))
        print("KEYERROR!")
    return Q


# TODO: implement logic
def choose_action(stateNow, Q, e_greedy):
    # first find the actions possible
    stateString = json.dumps(stateNow, sort_keys=True)
    try:
        qActions = Q[stateString]
    except KeyError:
        print("Q: {}".format(Q))
        print("StateNowStr: {}".format(stateNow))
        print("KEYERROR! Q action choose")
    actionChosen = keywithmaxActionval(qActions)
    # take random decision, if value between 0-1 is smaller than e greedy
    if random.random() < e_greedy:
        listKeys = list(qActions.keys())
        # remove the chosen action
        listKeys.remove(actionChosen)
        actionChosen = random.choice(listKeys)
        print("xxxxxxxxxxxChosen randomly")
    # take max value
    return actionChosen


### fucntions for calculating.. maybe outsource to "functions"
def get_paths(latencyDict, src, dst):
    '''
    Get all paths from src to dst using DFS algorithm
    '''
    if src == dst:
        # host target is on the same switch
        return [[src]]
    paths = []
    stack = [(src, [src])]
    while stack:
        (node, path) = stack.pop()
        for next in set(latencyDict[node].keys()) - set(path):
            if next is dst:
                paths.append(path + [next])
            else:
                stack.append((next, path + [next]))
    return paths


# can also be changed to BWs, or to hops
def get_link_cost(latencyDict, s1, s2):
    # only latency:
    ew = latencyDict[s2][s1]
    return ew


def get_path_cost(latencyDict, path):
    cost = 0
    for i in range(len(path) - 1):
        cost += get_link_cost(latencyDict, path[i], path[i + 1])
    return cost


# TODO: max function always gets last key!
def keywithmaxActionval(actions):

    """ a) create a list of the dict's keys and values;
        b) return the key with the max value"""
    v = list(actions.values())
    k = list(actions.keys())
    return k[v.index(max(v, key=scndElement))]

def scndElement(e):
    return e[1]

def calc_epsilon(steps):
    #return 0.507928 - 0.08 * math.log(steps)
    #return  0.507928 - 0.05993925*math.log(steps)
    return 0.1

# TODO: not ready
def calc_UCB(Q, currentState):
    currentStateStr = json.loads(currentState)
    actions = list(Q[currentStateStr].keys())
    # number of possible actions (K)
    t = actions.len()
    BiggestValueKey = keywithmaxActionval(actions)
    # if still not checked -> take it
    if Q[currentStateStr][keywithmaxActionval][0] == 0:
        t=0

#def overWritingFile():
#    with open('Q_array.json', 'w') as file:

def saveQ(Q):
    with open('../Q_array.json', 'a') as file:
        json.dump(Q, file)  # use `json.loads` to do the reverse

def saveQCurrent(Q):
    with open('../Q_array_current.json', 'w') as file:
        json.dump(Q, file)  # use `json.loads` to do the reverse

def saveRewardCsv(fileName, reward, timepoint):
    with open('../{}.csv'.format(fileName), 'a') as csvfile:
        fileWriter = csv.writer(csvfile, delimiter=',')
        fileWriter.writerow([timepoint, reward, time.time()])

def clearingSaveFile(fileName):
    open('../{}.csv'.format(fileName), 'w').close()

# hysteresis
# bwlevel in Mbit/s
def checkBwLevel(currentBw, prevBwLevel, levelList, tolerance = 0.2):
    # lower level
    if (currentBw < levelList[levelList.index(prevBwLevel)-1]):
        # over tolerance -> change bw level to lower level
        if abs((currentBw - prevBwLevel)) > tolerance and prevBwLevel > levelList[0]:
            currentBwLevel = getBwLevel(currentBw, levelList)
            if currentBwLevel != prevBwLevel:
                return currentBwLevel
    if (currentBw > prevBwLevel):
        # ovr tolerance -> change bw to higher level
        if (currentBw - prevBwLevel) > tolerance:
            currentBwLevel = getBwLevel(currentBw, levelList)
            if currentBwLevel != prevBwLevel:
                return currentBwLevel
    return prevBwLevel

def getCurrentCombinations(currentCombination, bwLevels, prevCombination, levelList, tolerance):
    currentCombWithBw = {}
    #print("BW LEVELS: {}".format(bwLevels))
    for flowId in currentCombination:
        if flowId in bwLevels:
            bwLevelCurrFloat = convertBitsInMBits(bwLevels[flowId])
        else:
            bwLevelCurrFloat = 0.0
        if flowId in prevCombination:
            bwLevelPrev = prevCombination[flowId][1]
            # check if state jump
            bwLevel = checkBwLevel(bwLevelCurrFloat, bwLevelPrev, levelList, tolerance)
        else:
            bwLevel = getBwLevel(bwLevelCurrFloat, levelList)
            print()
            # check if difference
        path = currentCombination[flowId]
        currentCombWithBw[flowId] = [path, bwLevel]
    #print("currentComb: {}".format(currentCombWithBw))
    return currentCombWithBw

# for three levels
def getBwLevel(bwLevel, levelList):
    if bwLevel < levelList[0]:
        return levelList[0]
    if bwLevel > levelList[0]  and bwLevel < levelList[1]:
        return levelList[1]
    if bwLevel > levelList[1]:
        return levelList[2]
    return levelList[0]

def convertBitsInMBits(bwBits):
    return bwBits / (10 ** 6)